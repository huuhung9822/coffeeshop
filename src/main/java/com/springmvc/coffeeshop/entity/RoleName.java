package com.springmvc.coffeeshop.entity;

public enum RoleName {
	ADMIN,
	USER
}
