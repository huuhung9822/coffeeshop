package com.springmvc.coffeeshop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SALE", catalog="coffeeshop")
public class Sale implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SALE_ID")
	private Long id;
	
	@Column(name="SALE_NAME")
	private String name;
	
	@Column(name = "SALE_PERCENT")
	private int salePercent;

	public Sale() {
		super();
	}

	public Sale(Long id, String name, int salePercent) {
		super();
		this.id = id;
		this.name = name;
		this.salePercent = salePercent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalePercent() {
		return salePercent;
	}

	public void setSalePercent(int salePercent) {
		this.salePercent = salePercent;
	}
	
	
}
