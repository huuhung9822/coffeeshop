package com.springmvc.coffeeshop.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PERSISTENT_LOGIN", catalog="coffeeshop")
public class PersistentLogin implements Serializable{
	
//	private String series;
//	private String userName;
//	private String token;
//	private Date last_used;
	@Id
	@Column(name="SERIES")
	private String series;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@Column(name="TOKEN")
	private String token;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date last_used;

	public PersistentLogin(String series, String userName, String token, Date last_used) {
		super();
		this.series = series;
		this.userName = userName;
		this.token = token;
		this.last_used = last_used;
	}

	public PersistentLogin() {
		super();
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLast_used() {
		return last_used;
	}

	public void setLast_used(Date last_used) {
		this.last_used = last_used;
	}
	
	
	

}
