package com.springmvc.coffeeshop.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCTS", catalog = "coffeeshop")
public class Product implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PRODUCT_ID")
	private Long id;
	
	@Column(name = "PRODUCT_NAME")
	private String name;
	
	@Column(name ="PRICE")
	private int price;
	
	@Column (name = "PRODUCT_STATUS")
	private Boolean status;
	
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.DETACH)
	private Category category;
	
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.DETACH)
	@JoinColumn(name = "PRODUCT_SALE")
	private Sale sale;

	public Product() {
		super();
	}

	public Product(Long id, String name, int price, Boolean status, Category category, Sale sale) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.status = status;
		this.category = category;
		this.sale = sale;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}
	
	
}
