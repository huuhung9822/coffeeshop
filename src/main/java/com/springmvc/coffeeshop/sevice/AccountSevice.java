package com.springmvc.coffeeshop.sevice;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.springmvc.coffeeshop.dao.impl.AccountDaoImpl;
import com.springmvc.coffeeshop.dao.impl.RoleDaoImpl;
import com.springmvc.coffeeshop.entity.Account;
import com.springmvc.coffeeshop.entity.Role;

@Transactional
@Service
public class AccountSevice {

	@Autowired
	private AccountDaoImpl accountDao;
	
	private RoleDaoImpl roleDao;
	
	private PasswordEncoder passwordEncoder;
	
	public List<Account> getAll()
	{
		return accountDao.getAll();
	}
	
	public Account getByUserName(String id)
	{
		return accountDao.getById(id);
	}
	
	public void saveAccount(Account account)
	{
		account.setPassword(passwordEncoder.encode(account.getPassword()));
		accountDao.create(account);
	}
	
	public void updateAccount(String accountName, Account account)
	{
		Account entity = accountDao.getById(accountName);
		if(entity!= null)
		{
			if(!account.getPassword().equals(passwordEncoder.encode(entity.getPassword())))
			{
				entity.setPassword(passwordEncoder.encode(account.getPassword()));
				accountDao.edit(entity);
			}
		}
	}
	
	public void deleteAccount(Account account)
	{
		if(account != null)
		{
			accountDao.remove(account);
		}
	}
	
	public void deleteById(String id)
	{
		if(id!=null)
		{
			accountDao.remove(id);
		}
	}
	
	public boolean isAccountUnique(String accountName)
	{
		Account account = accountDao.getById(accountName);
		return (account == null);
	}
	
	public List<Role> findAllRoles()
	{
		return roleDao.getAll();
	}
	
	public Role getRoleById(Long roleId)
	{
		return roleDao.getById(roleId);
	}
}
