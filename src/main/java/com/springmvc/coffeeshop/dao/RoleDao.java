package com.springmvc.coffeeshop.dao;

import com.springmvc.coffeeshop.entity.Role;

public interface RoleDao extends GenericDao<Role, Long>{

}
