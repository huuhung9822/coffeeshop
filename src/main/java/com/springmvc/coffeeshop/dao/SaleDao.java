package com.springmvc.coffeeshop.dao;

import com.springmvc.coffeeshop.entity.Sale;

public interface SaleDao extends GenericDao<Sale, Long>{

}
