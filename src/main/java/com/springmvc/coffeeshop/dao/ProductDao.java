package com.springmvc.coffeeshop.dao;

import com.springmvc.coffeeshop.entity.Product;

public interface ProductDao extends GenericDao<Product, Long>{

}
