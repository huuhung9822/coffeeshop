package com.springmvc.coffeeshop.dao;

import com.springmvc.coffeeshop.entity.Account;

public interface AccountDao extends GenericDao<Account, String>{

}
