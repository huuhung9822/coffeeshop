package com.springmvc.coffeeshop.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

public interface GenericDao <T, K extends Serializable>{
	public T create(T entity);
	public Boolean edit(T entity);
	public Boolean remove(T entity);
	public T getById(K id);
	public List<T> getAll();
	public Boolean remove(K id);
	public Criteria createEntityCriteria();
	public Session getSession();
}
