package com.springmvc.coffeeshop.dao;

import java.io.Serializable;

import com.springmvc.coffeeshop.entity.Category;

public interface CategoryDao extends GenericDao<Category, Long>{

}
