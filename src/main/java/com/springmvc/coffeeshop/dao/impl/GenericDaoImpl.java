package com.springmvc.coffeeshop.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.springmvc.coffeeshop.dao.GenericDao;

public class GenericDaoImpl<T, K extends Serializable> implements GenericDao<T, K> {

	private Class<T> entityClass;

	@Autowired
	private SessionFactory sessionFactory;

	private GenericDaoImpl() {

	}

	public GenericDaoImpl(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public T create(T entity) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Error during create " + entity.toString());
		} finally {
			session.close();
		}
		return entity;
	}

	public Boolean edit(T entity) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			session.update(entity);
			session.getTransaction().commit();
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println("Error during edit " + entity.toString());
			return Boolean.FALSE;
		} finally {
			session.close();
		}
	}

	public Boolean remove(T entity) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			session.delete(entity);
			session.getTransaction().commit();
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println("Error during remove " + entity.toString());
			return Boolean.FALSE;
		} finally {
			session.close();
		}
	}

	public T getById(K id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			return session.find(entityClass, id);
		} catch (Exception e) {
			System.out.println("Error during finding. . .");
		} finally {
			session.close();
		}
		return null;
	}

	public List<T> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("from " + entityClass.getName());
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("Error during find " + entityClass.getName());
		} finally {
			session.close();
		}
		return null;
	}

	public Boolean remove(K id) {
		Session session = this.sessionFactory.getCurrentSession();
		session.beginTransaction();
		T entity = (T) session.load(entityClass, id);
		if (null != entity) {
			session.delete(entity);
			session.getTransaction().commit();
			session.close();
			return Boolean.TRUE;
		}
		session.close();
		return Boolean.FALSE;
	}
	public Criteria createEntityCriteria() {
		return getSession().createCriteria(entityClass);
	}
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
