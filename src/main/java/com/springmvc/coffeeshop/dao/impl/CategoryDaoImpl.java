package com.springmvc.coffeeshop.dao.impl;

import org.springframework.stereotype.Repository;

import com.springmvc.coffeeshop.dao.CategoryDao;
import com.springmvc.coffeeshop.entity.Category;

@Repository
public class CategoryDaoImpl extends GenericDaoImpl<Category, Long> implements CategoryDao{

	public CategoryDaoImpl() {
		super(Category.class);
		// TODO Auto-generated constructor stub
	}

}
