package com.springmvc.coffeeshop.dao.impl;

import org.springframework.stereotype.Repository;

import com.springmvc.coffeeshop.dao.RoleDao;
import com.springmvc.coffeeshop.entity.Role;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, Long> implements RoleDao{

	public RoleDaoImpl() {
		super(Role.class);
		// TODO Auto-generated constructor stub
	}

}
