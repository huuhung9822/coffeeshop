package com.springmvc.coffeeshop.dao.impl;

import org.springframework.stereotype.Repository;

import com.springmvc.coffeeshop.dao.SaleDao;
import com.springmvc.coffeeshop.entity.Sale;

@Repository
public class SaleDaoImpl extends GenericDaoImpl<Sale, Long> implements SaleDao{

	public SaleDaoImpl() {
		super(Sale.class);
		// TODO Auto-generated constructor stub
	}
	

}
