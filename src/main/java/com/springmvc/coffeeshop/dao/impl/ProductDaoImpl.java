package com.springmvc.coffeeshop.dao.impl;

import org.springframework.stereotype.Repository;

import com.springmvc.coffeeshop.dao.ProductDao;
import com.springmvc.coffeeshop.entity.Product;

@Repository
public class ProductDaoImpl extends GenericDaoImpl<Product, Long> implements ProductDao{

	public ProductDaoImpl() {
		super(Product.class);
		// TODO Auto-generated constructor stub
	}

}
