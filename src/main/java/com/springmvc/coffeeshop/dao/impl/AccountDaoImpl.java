package com.springmvc.coffeeshop.dao.impl;

import org.springframework.stereotype.Repository;

import com.springmvc.coffeeshop.dao.AccountDao;
import com.springmvc.coffeeshop.entity.Account;

@Repository
public class AccountDaoImpl extends GenericDaoImpl<Account, String> implements AccountDao{

	public AccountDaoImpl() {
		super(Account.class);
		// TODO Auto-generated constructor stub
	}
	
}
